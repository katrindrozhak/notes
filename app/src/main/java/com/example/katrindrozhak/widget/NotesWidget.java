package com.example.katrindrozhak.widget;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.TaskStackBuilder;
import android.widget.RemoteViews;

import com.example.katrindrozhak.notes.Constants;
import com.example.katrindrozhak.notes.MainActivity;
import com.example.katrindrozhak.notes.NoteActivity;
import com.example.katrindrozhak.notes.R;

/**
 * Created by katrindrozhak on 13.03.17.
 */

public class NotesWidget extends AppWidgetProvider {


    @Override
    public void onReceive(final Context context, Intent intent) {

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(
                new ComponentName(context, getClass()));
        this.onUpdate(context, appWidgetManager, appWidgetIds);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widgetList);
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int widgetId : appWidgetIds) {
            RemoteViews views = initViews(context, appWidgetManager, widgetId);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);
            views.setOnClickPendingIntent(R.id.widget_layout, pendingIntent);

            Intent intentShowDetail = new Intent(context, NoteActivity.class);
            Intent intentChangeColor = new Intent(context, WidgetActivity.class);

            PendingIntent pendingIntentShowDetail = TaskStackBuilder.create(context)
                    .addNextIntentWithParentStack(intentShowDetail)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            PendingIntent pendingIntentChangeColor = TaskStackBuilder.create(context)
                    .addNextIntentWithParentStack(intentChangeColor)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            views.setPendingIntentTemplate(R.id.widgetList, pendingIntentShowDetail);
            views.setOnClickPendingIntent(R.id.color_picker_btn, pendingIntentChangeColor);

            SharedPreferences pref = context.getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
            int defaultColor =  R.color.colorPrimary;


            int color = pref.getInt(Constants.CURRENT_COLOR, defaultColor);
            views.setInt(R.id.widget_layout, "setBackgroundColor", color);

            appWidgetManager.updateAppWidget(widgetId, views);

        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private RemoteViews initViews(Context context, AppWidgetManager appWidgetManager, int widgetId) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.notes_widget);

        Intent intent = new Intent(context, WidgetService.class);
        intent.putExtra(appWidgetManager.EXTRA_APPWIDGET_ID, widgetId);

        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        remoteViews.setRemoteAdapter(widgetId, R.id.widgetList, intent);

        return remoteViews;
    }
}



