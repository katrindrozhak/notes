package com.example.katrindrozhak.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.katrindrozhak.notes.Constants;
import com.example.katrindrozhak.notes.R;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

/**
 * Created by katrindrozhak on 15.03.17.
 */

public class WidgetActivity extends AppCompatActivity {
    private int currentBackgroundColor = 0xffffffff;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeBackgroundColor(currentBackgroundColor);

        ColorPickerDialogBuilder.with(this)
                .setTitle(getString(R.string.title_choose_color))
                .initialColor(currentBackgroundColor)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12).setOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int i) {
            }
        }).setPositiveButton(getString(R.string.dialog_btn_ok), new ColorPickerClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, Integer[] integers) {
                changeBackgroundColor(i);
                SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_FILE_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(Constants.CURRENT_COLOR, currentBackgroundColor);
                editor.apply();

                updateMyWidgets(getApplicationContext());

                finish();
            }
        }).setNegativeButton(getString(R.string.dialog_btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).build().show();
    }

    private void changeBackgroundColor(int selectedColor) {
        currentBackgroundColor = selectedColor;
    }

    public void updateMyWidgets(Context context) {
        Intent updateIntent = new Intent();
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        context.sendBroadcast(updateIntent);
    }

}
