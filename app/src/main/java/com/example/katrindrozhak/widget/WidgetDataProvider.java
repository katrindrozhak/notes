package com.example.katrindrozhak.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.example.katrindrozhak.notes.Constants;
import com.example.katrindrozhak.notes.Note;
import com.example.katrindrozhak.notes.R;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by katrindrozhak on 13.03.17.
 */

@SuppressLint("NewApi")
public class WidgetDataProvider implements RemoteViewsService.RemoteViewsFactory {

    List<Note> noteList = new ArrayList();
    private Context context = null;

    public WidgetDataProvider(Context context, Intent intent) {
        this.context = context;
    }

    private void initCursor() {
        if (!noteList.isEmpty()) {
            noteList.clear();
        }
        noteList = new Select().from(Note.class).queryList();
    }

    @Override
    public void onCreate() {
        initCursor();
    }

    @Override
    public void onDataSetChanged() {
        initCursor();
    }

    @Override
    public void onDestroy() {
        noteList.clear();
    }

    @Override
    public int getCount() {
        return noteList.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), android.R.layout.simple_list_item_1);
        remoteViews.setTextViewText(android.R.id.text1, noteList.get(position).getTitle());
        remoteViews.setTextColor(android.R.id.text1, ContextCompat.getColor(context, R.color.primaryLight));

        final Intent fillIntent = new Intent();
        fillIntent.setAction(Constants.CLICK_ACTION);
        final Bundle bundle = new Bundle();
        bundle.putInt(Constants.KEY_NOTE_ID, noteList.get(position).getId());

        fillIntent.putExtras(bundle);
        remoteViews.setOnClickFillInIntent(android.R.id.text1, fillIntent);

        return remoteViews;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

}
