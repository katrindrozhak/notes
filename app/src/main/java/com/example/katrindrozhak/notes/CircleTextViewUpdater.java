package com.example.katrindrozhak.notes;


import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by katrindrozhak on 18.03.17.
 */

public class CircleTextViewUpdater implements TextWatcher {

    private TextView receiverTextView;
    ColorsArray colorsArray = new ColorsArray();
    String[] arrayCol = colorsArray.colors;

    public CircleTextViewUpdater(TextView receiver) {
        receiverTextView = receiver;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        receiverTextView.setText((s.length() > 0) ? String.valueOf(s.charAt(0)).toUpperCase() : "");

        if (s.length() > 0) {
            GradientDrawable gradientDrawable = (GradientDrawable) receiverTextView.getBackground();
            gradientDrawable.setColor(Color.parseColor("#" + arrayCol[s.charAt(0) % arrayCol.length]));
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
