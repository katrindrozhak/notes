package com.example.katrindrozhak.notes;

/**
 * Created by katrindrozhak on 17.03.17.
 */

public class ColorsArray {
    public final String[] colors = {

            "FFCDD2", "EF5350", "B71C1C",                      //reds  3
            "F06292", "EC407A", "880E4F",              //pinks 3
            "BA68C8", "7B1FA2", "4A148C",   //purples 3
            "673AB7", "4527A0", "311B92",  //deep purples 3
            "3F51B5", "1A237E",               //indigo 2
            "1E88E5", "01579B", "0097A7", "006064", //blue 4
            "009688", "00796B", "004D40",     //teal 3
            "009688", "00695C",        //teal 2
            "43A047", "1B5E20",       //green 2
            "689F38", "827717", "F4FF81",     // lime 3
            "FBC02D", "F9A825", "F57F17", //yellow 3
            "FFA000", "FF8F00",    //amber 2
            "E65100", "FFD180",   //orange
            "FF7043", "DD2600",   //deep orange
            "8D6E63", "3E2723",                  //brown
            "616161", "424242",                           //grey
            "546E7A", "37474F"                           //blue grey

    };
}
