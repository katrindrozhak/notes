package com.example.katrindrozhak.notes;

import android.location.Location;

/**
 * Created by katrindrozhak on 21.03.17.
 */

interface LocationListener {

    void onReceiveLocation(Location location);
}
