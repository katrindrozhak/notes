package com.example.katrindrozhak.notes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.katrindrozhak.widget.WidgetActivity;

public class MainActivity extends AppCompatActivity {
    private NotesFragment fragment;
    private ActionMode actionMode;
    private int currentListItemIndex;
    private boolean isMultiSelect = false;
    private Menu menuInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragment = new NotesFragment();
        fragment = (NotesFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        fragment.refresh();

        ItemClickSupport.addTo(fragment.recyclerView).setOnItemClickSupport(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView rv, int position, View v) {
                currentListItemIndex = position;
                if (isMultiSelect) {
                    multiSelect(currentListItemIndex);
                    visibleItemMenu(menuInfo);
                } else {
                    refactorNote(v, currentListItemIndex);
                }
            }
        });
        ItemClickSupport.addTo(fragment.recyclerView).setOnItemLongClickSupport(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView rv, int position, View v) {
                currentListItemIndex = position;

                if (actionMode == null && !isMultiSelect) {
                    actionMode = startActionMode(callback);
                    fragment.showMenu(false);
                    multiSelect(currentListItemIndex);
                    isMultiSelect = true;
                }
                return true;
            }

        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNote(view);

            }
        });

    }

    android.view.ActionMode.Callback callback = new android.view.ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.context_menu, menu);
            MenuItem menuItem = menu.findItem(R.id.action_share);
            menuInfo = menu;
            menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    String text = getString(R.string.extra_text_for_intent) + " " + fragment.getNoteAt(currentListItemIndex).description;
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, text);
                    shareIntent.setType(Constants.SET_TYPE_TEXT);
                    startActivity(shareIntent);
                    return true;
                }
            });

            return true;
        }

        @Override
        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(final android.view.ActionMode mode, final MenuItem item) {

            switch (item.getItemId()) {
                case R.id.delete_item:
                    DeleteDialogFragment deleteDialogFragment = new DeleteDialogFragment();
                    deleteDialogFragment.dialogListener = new DeleteDialogListener() {
                        @Override
                        public void onClickButton() {
                            fragment.deleteMultipleChoice();
                            fragment.refresh();
                            WidgetActivity widgetActivity = new WidgetActivity();
                            widgetActivity.updateMyWidgets(getApplicationContext());
                            mode.finish();
                        }
                    };
                    deleteDialogFragment.show(getSupportFragmentManager(), Constants.TEXT);

                    return true;
                case R.id.menu_edit:
                    refactorNote(currentListItemIndex);
                    mode.finish();
                    return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(android.view.ActionMode mode) {
            Log.d(Constants.LOG_TAG, Constants.LOG_DESTROY);
            actionMode = null;
            isMultiSelect = false;
            fragment.resetSelection();
            fragment.showMenu(true);
        }
    };

    public void createNote(View view) {
        Intent intent = new Intent(this, NoteActivity.class);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                new Pair<>(view.findViewById(R.id.fab), getString(R.string.transition_name_fab)));
        ActivityCompat.startActivityForResult(this, intent, 0, options.toBundle());
    }

    public void refactorNote(View view, int position) {
        Note refactorNote = fragment.getNoteAt(position);

        Intent intent = new Intent(this, NoteActivity.class);
        intent.putExtra(Constants.KEY_NOTE_ID, refactorNote.id);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                new Pair<>(view.findViewById(R.id.circle_item), getString(R.string.transition_name_circle)),
                new Pair<>(view.findViewById(R.id.title_item), getString(R.string.transition_name_title)));
        ActivityCompat.startActivityForResult(this, intent, position, options.toBundle());
    }

    public void refactorNote(int position) {
        Note refactorNote = fragment.getNoteAt(position);

        Intent intent = new Intent(this, NoteActivity.class);
        intent.putExtra(Constants.KEY_NOTE_ID, refactorNote.id);

        startActivityForResult(intent, position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fragment.refresh();
    }

    public void multiSelect(int position) {
        if (actionMode != null) {
            fragment.multiSelect(position);
            fragment.refresh();
        }
    }

    public void visibleItemMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_edit);
        item.setVisible(fragment.adapter.selectedNotes.size() <= 1);
    }
}
