package com.example.katrindrozhak.notes;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by katrindrozhak on 28.02.17.
 */

public class ItemClickSupport {

    private final RecyclerView recyclerView;
    private OnItemClickListener itemClickListener;
    private OnItemLongClickListener itemLongClickListener;


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                RecyclerView.ViewHolder holder = recyclerView.getChildViewHolder(v);
                itemClickListener.onItemClicked(recyclerView, holder.getAdapterPosition(), v);
            }
        }

    };

    private View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {

            if (itemLongClickListener != null) {
                RecyclerView.ViewHolder holder = recyclerView.getChildViewHolder(v);

                return itemLongClickListener.onItemLongClicked(recyclerView, holder.getAdapterPosition(), v);
            }

            return false;
        }
    };


    private RecyclerView.OnChildAttachStateChangeListener attachStateChangeListener =
            new RecyclerView.OnChildAttachStateChangeListener() {
                @Override
                public void onChildViewAttachedToWindow(View view) {

                    if (itemClickListener != null) {
                        view.setOnClickListener(onClickListener);
                    }
                    if (itemLongClickListener != null) {
                        view.setOnLongClickListener(onLongClickListener);
                    }

                }

                @Override
                public void onChildViewDetachedFromWindow(View view) {

                }
            };

    private ItemClickSupport(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        recyclerView.setTag(R.id.item_click_support, this);
        recyclerView.addOnChildAttachStateChangeListener(attachStateChangeListener);
    }

    static ItemClickSupport addTo(RecyclerView rv) {
        ItemClickSupport support = (ItemClickSupport) rv.getTag(R.id.item_click_support);

        if (support == null) {
            support = new ItemClickSupport(rv);
        }
        return support;
    }

    ItemClickSupport setOnItemClickSupport(OnItemClickListener listener) {
        this.itemClickListener = listener;
        return this;
    }

    ItemClickSupport setOnItemLongClickSupport(OnItemLongClickListener listener) {
        this.itemLongClickListener = listener;
        return this;
    }

    interface OnItemClickListener {
        void onItemClicked(RecyclerView rv, int position, View v);
    }

    interface OnItemLongClickListener {
        boolean onItemLongClicked(RecyclerView rv, int position, View v);
    }
}
