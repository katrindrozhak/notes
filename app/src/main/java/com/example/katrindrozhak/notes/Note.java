package com.example.katrindrozhak.notes;

import android.net.Uri;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.provider.ContentUri;
import com.raizlabs.android.dbflow.annotation.provider.TableEndpoint;
import com.raizlabs.android.dbflow.structure.provider.BaseProviderModel;
import com.raizlabs.android.dbflow.structure.provider.ContentUtils;

import java.util.Date;

/**
 * Created by katrindrozhak on 14.02.17.
 */

@TableEndpoint(name = Note.NAME, contentProvider = MyDatabase.class)
@Table(database = MyDatabase.class)
public class Note extends BaseProviderModel {

    public static final String NAME = "Note";
    @ContentUri(path = NAME, type = ContentUri.ContentType.VND_MULTIPLE + NAME)
    public static final Uri CONTENT_URI = ContentUtils.buildUriWithAuthority(MyDatabase.AUTHORITY + "/" + NAME);


    @Column
    @PrimaryKey
    int id = new Date().hashCode();

    @Column
    String title;

    @Column(name = "name")
    String description;

    @Column
    Date date;

    @Column
    Date updateDate;

    @Column
    double latitude;

    @Column
    double longitude;

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    Note() {
        updateDate = date = new Date();
    }

    @Override
    public boolean equals(Object obj) {
        Note anotherNote = (Note) obj;
        return this.id == anotherNote.id;
    }

    @Override
    public Uri getDeleteUri() {
        return Note.CONTENT_URI;
    }

    @Override
    public Uri getInsertUri() {
        return Note.CONTENT_URI;
    }

    @Override
    public Uri getUpdateUri() {
        return Note.CONTENT_URI;
    }

    @Override
    public Uri getQueryUri() {
        return Note.CONTENT_URI;
    }

    public String getColor() {
        String color = "#33b5e5";
        return color;
    }
}
