package com.example.katrindrozhak.notes;

import android.app.Fragment;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.katrindrozhak.widget.WidgetActivity;
import com.google.android.gms.maps.model.LatLng;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.Date;

/**
 * Created by katrindrozhak on 28.02.17.
 */

public class CreateNoteFragment extends Fragment {
    private EditText title;
    private EditText description;
    private TextView circleTv;
    private int noteId;
    private Note currentNote;
    Location lastLocation;
    LocationListener locationListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);
        title = (EditText) view.findViewById(R.id.titleText);
        description = (EditText) view.findViewById(R.id.descriptionText);
        circleTv = (TextView) view.findViewById(R.id.circle_item);

        title.addTextChangedListener(new CircleTextViewUpdater(circleTv));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.note_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_item:

                if (!isValid(title)) return false;
                if (!isValid(description)) return false;

                Note note = currentNote == null ? new Note() : currentNote;
                note.title = title.getText().toString();
                note.description = description.getText().toString();
                note.updateDate = new Date();

                if (lastLocation != null) {

                    note.latitude = lastLocation.getLatitude();
                    note.longitude = lastLocation.getLongitude();
                } else {
                    note.latitude = 0;
                    note.longitude = 0;
                }

                note.save();

                WidgetActivity widgetActivity = new WidgetActivity();
                widgetActivity.updateMyWidgets(getActivity());
                getActivity().finishAfterTransition();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
        currentNote = new Select().from(Note.class).where(Note_Table.id.eq(noteId)).querySingle();
        title.setText(currentNote != null ? currentNote.title : null);
        description.setText(currentNote.description);
        circleTv.setText(String.valueOf(currentNote.title.charAt(0)).toUpperCase());
        Location location = new Location("");
        location.setLatitude(currentNote.latitude);
        location.setLongitude(currentNote.longitude);
        locationListener.onReceiveLocation(location);
    }

    public boolean isValid(EditText text) {
        if (text.getText().toString().length() == 0) {
            text.setError(getResources().getString(R.string.error_message_for_et));
            return false;
        }
        return true;
    }

    public LatLng latLng(Location location) {
        lastLocation = location;
        return new LatLng(location.getLatitude(), location.getLongitude());
    }
}
