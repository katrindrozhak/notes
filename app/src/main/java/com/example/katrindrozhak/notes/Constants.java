package com.example.katrindrozhak.notes;

/**
 * Created by katrindrozhak on 01.03.17.
 */

public class Constants {
    public static final String TEXT = "text";
    public static final String SET_TYPE_TEXT = "text/plain";
    public static final String LOG_TAG = "myLogs";
    public static final String LOG_DESTROY = "destroy";
    public static final String KEY_NOTE_ID = "noteId";
    public static final String CURRENT_COLOR = "currentColor";
    public static final String PREFS_FILE_NAME = "app_shared_preferences";
    public static final String CLICK_ACTION = "com.example.katrindrozhak.widget.CLICK_ACTION";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

}
