package com.example.katrindrozhak.notes;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.katrindrozhak.widget.WidgetActivity;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by katrindrozhak on 14.02.17.
 */

public class NotesFragment extends Fragment {
    EmptyRecyclerView recyclerView;
    NoteListAdapter adapter;
    private Menu menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.content_fragment, null);
        recyclerView = (EmptyRecyclerView) view.findViewById(R.id.recyclerView);
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setEmptyView(view.findViewById(R.id.tv_empty));
        List<Note> noteSQL = new ArrayList<>();
        adapter = new NoteListAdapter(this.getActivity(), (ArrayList<Note>) noteSQL);
        recyclerView.setAdapter(adapter);

        return view;
    }

    void resetSelection() {
        adapter.selectedNotes.clear();
        refresh();
    }

    void refresh() {
        try {
            adapter.noteData = new Select().from(Note.class).queryList();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

    public Note getNoteAt(int index) {
        return adapter.noteData.get(index);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                DeleteDialogFragment dialogFragment = new DeleteDialogFragment();
                dialogFragment.dialogListener = new DeleteDialogListener() {
                    @Override
                    public void onClickButton() {

                        Delete.table(Note.class);
                        refresh();
                    }
                };
                dialogFragment.show(getFragmentManager(), Constants.TEXT);

                WidgetActivity widgetActivity = new WidgetActivity();
                widgetActivity.updateMyWidgets(getActivity());
                return true;

            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void multiSelect(int position) {
        Note selectedNote = adapter.noteData.get(position);
        if (adapter.selectedNotes.contains(selectedNote)) {
            adapter.selectedNotes.remove(selectedNote);
        } else {
            adapter.selectedNotes.add(selectedNote);
        }
    }

    public void deleteMultipleChoice() {
        for (Note note : adapter.selectedNotes) {

            SQLite.delete().from(Note.class).where(Note_Table.id.eq(note.id)).execute();
        }
        resetSelection();
    }

    public void showMenu(boolean willShow) {
        menu.findItem(R.id.delete).setVisible(willShow);
    }
}
