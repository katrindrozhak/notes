package com.example.katrindrozhak.notes;

/**
 * Created by katrindrozhak on 14.02.17.
 */

public interface DeleteDialogListener {
    void onClickButton();
}
