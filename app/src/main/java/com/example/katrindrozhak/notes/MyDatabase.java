package com.example.katrindrozhak.notes;

import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.annotation.provider.ContentProvider;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;

/**
 * Created by katrindrozhak on 14.02.17.
 */
@ContentProvider(authority = MyDatabase.AUTHORITY,
        database = MyDatabase.class,
        baseContentUri = MyDatabase.BASE_CONTENT_URI)
@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {
    public static final String NAME = "MyDatabase";

    public static final int VERSION = 3;
    public static final String AUTHORITY = "com.example.katrindrozhak.notes";
    public static final String BASE_CONTENT_URI = "content://";

    @Migration(version = 2, database = MyDatabase.class)
    public static class Migration1 extends AlterTableMigration<Note> {
        public Migration1(Class<Note> table) {
            super(table);
        }

        @Override
        public void onPreMigrate() {
            super.onPreMigrate();
            addColumn(SQLiteType.TEXT, "updateDate");
        }
    }

    @Migration(version = 3, database = MyDatabase.class)
    public static class Migration2 extends AlterTableMigration<Note> {

        public Migration2(Class<Note> table) {
            super(table);
        }

        @Override
        public void onPreMigrate() {
            super.onPreMigrate();
            addColumn(SQLiteType.REAL, "latitude");
            addColumn(SQLiteType.REAL, "longitude");
        }
    }
}


