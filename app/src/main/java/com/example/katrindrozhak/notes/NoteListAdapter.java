package com.example.katrindrozhak.notes;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by katrindrozhak on 14.02.17.
 */

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.ViewHolder> {

    private Context context;

    List<Note> noteData = new ArrayList<>();
    List<Note> selectedNotes = new ArrayList<>();
    private LayoutInflater inflater;

    NoteListAdapter(Context context, ArrayList<Note> noteData) {
        this.context = context;
        this.noteData = noteData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ColorsArray colorsArray = new ColorsArray();
        Note note = getNote(position);
        String[] arrayCol = colorsArray.colors;
        holder.title.setText(note.title);
        holder.dateAndTime.setText(DateTime.formatted(note.updateDate));

        if (selectedNotes.contains(noteData.get(position))) {
            holder.container.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        } else {
            holder.container.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }

        GradientDrawable gradientDrawable = (GradientDrawable) holder.circle.getBackground();
        gradientDrawable.setColor(Color.parseColor("#" + arrayCol[note.title.charAt(0) % arrayCol.length]));

        holder.circle.setText(String.valueOf(note.title.charAt(0)).toUpperCase());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return noteData.size();
    }


    private Note getNote(int position) {
        return noteData.get(position);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView dateAndTime;
        RelativeLayout container;
        TextView circle;

        ViewHolder(View itemView) {
            super(itemView);
            container = (RelativeLayout) itemView.findViewById(R.id.itemLayout);
            title = (TextView) itemView.findViewById(R.id.title_item);
            dateAndTime = (TextView) itemView.findViewById(R.id.date_and_time);
            circle = (TextView) itemView.findViewById(R.id.circle_item);
            itemView.setTag(itemView);
        }
    }
}
